package com.company;

import com.company.annotations.FinishTests;
import com.company.annotations.MyTest;
import com.company.annotations.StartTests;

@StartTests(startMessage = "My tests :)")
@FinishTests
public class Tests {
    @MyTest(isIgnored = false)
    void testIsPassing() {
    }

    @MyTest
    void testIsPassing2() {
    }

    @MyTest(isIgnored = true)
    void testIsIgnored() {
    }

    @MyTest
    void testThrowsException() {
        throw new RuntimeException("This test should fail!");
    }
}
