package com.company;

import com.company.annotations.FinishTests;
import com.company.annotations.MyTest;
import com.company.annotations.StartTests;

import java.lang.reflect.Method;

public class Main {
    public static void selectClass(Class className) {
        int passed = 0;
        int failed = 0;
        int total = 0;
        int ignored = 0;
        Class<StartTests> tests = null;
        try {
            tests = className;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }


        if (tests.isAnnotationPresent(StartTests.class)) {
            StartTests startTests = tests.getAnnotation(StartTests.class);
            System.out.println(startTests.startMessage());
            System.out.println();
        }

        for (Method method : tests.getDeclaredMethods()) {
            if (method.isAnnotationPresent(MyTest.class)) {
                MyTest test = method.getAnnotation(MyTest.class);
                if (!test.isIgnored()) {
                    try {
                        method.invoke(tests.newInstance());
                        System.out.printf("Test %s: '%s' passed %n", ++total, method.getName());
                        passed++;
                    } catch (Throwable ex) {
                        System.out.printf("Test %s: '%s' failed: %s %n", ++total, method.getName(), ex.getCause());
                        failed++;
                    }
                } else {
                    System.out.printf("Test %s: '%s' was ignored %n", ++total, method.getName());
                    ignored++;
                }
            }
        }
        if (tests.isAnnotationPresent(FinishTests.class)) {
            FinishTests finishTests = tests.getAnnotation(FinishTests.class);
            System.out.println();
            System.out.println(finishTests.finishMessage());

        }
        System.out.printf("%nTotal: %d %n" +
                "   Passed: %d %n" +
                "   Failed: %d %n" +
                "   Ignored: %d %n", total, passed, failed, ignored);

    }

    public static void main(String[] args) throws Exception {
        selectClass(Tests.class);
    }
}
